
import { IsNotEmpty, Matches } from 'class-validator';
import { IsComparePassword } from '../pipes/validation.pipe';
import { IChangePassword } from 'src/interfaces/app.interface';

export class ChangePasswordModel implements IChangePassword {

    @IsNotEmpty()
    @Matches(/^[A-z0-9]{6,15}$/)
    // tslint:disable-next-line: variable-name
    old_pass: string;

    @IsNotEmpty()
    @Matches(/^[A-z0-9]{6,15}$/)
    // tslint:disable-next-line: variable-name
    new_pass: string;

    @IsNotEmpty()
    @IsComparePassword('new_pass')
    // tslint:disable-next-line: variable-name
    cnew_pass: string;

}
