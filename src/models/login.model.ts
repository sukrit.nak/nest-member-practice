import { IsNotEmpty, IsEmail } from 'class-validator';
import { ILogin } from '../interfaces/app.interface';

export class LoginModel implements ILogin {

    @IsNotEmpty()
    @IsEmail()
    email: string;

    @IsNotEmpty()
    password: string;

    remember: boolean;

}
