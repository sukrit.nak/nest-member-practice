import { LoginModel } from './../models/login.model';
import { Controller, Get, Post, Body, BadRequestException } from '@nestjs/common';
import { RegisterModel } from '../models/register.model';
import { ValidationPipe } from '../pipes/validation.pipe';
import { AppService } from '../services/app.service';

@Controller('api/account')
export class AccountController {

    constructor(private service: AppService) { }

    @Post('register') // ลงทะเบียน
    register(@Body(ValidationPipe) body: RegisterModel) {
        return this.service.onRegister(body);
    }

    @Post('login')
    login(@Body(ValidationPipe) body: LoginModel) {

        return this.service.onLogin(body);
    }
}