import { RoleAccount } from '../interfaces/app.interface';
import { Controller, Get, UseGuards, Req, Post, Body, Query, Param, Put, Delete } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Request } from 'express';
import { IMemberDocument } from '../interfaces/member.interface';
import { ProfileModel } from '../models/profile.model';
import { ValidationPipe } from '../pipes/validation.pipe';
import { MemberService } from '../services/member.service';
import { ChangePasswordModel } from '../models/change-password.model';
import { SearchModel } from '../models/search.model';
import { CreateMemberModel, ParamMemberModel, UpdateMemberModel } from '../models/member.model';
import { RoleGuard } from '../guards/role.guard';

@Controller('api/member')
// @UseGuards(AuthGuard('bearer'))
@UseGuards(AuthGuard('jwt'))
export class MemberController {
    constructor(private service: MemberService) { }

    @Get('data')
    getUserLogin(@Req() req: Request) {
        const userLogin: IMemberDocument = req.user as any;
        userLogin.image = userLogin.image ? 'http://localhost:3000' + userLogin.image + '?ver=' + Math.random() : '';
        userLogin.password = '';
        return userLogin;
    }

    @Post('profile')
    updateProfile(@Req() req: Request, @Body(new ValidationPipe()) body: ProfileModel) {
        return this.service.onUpdateProfile(req.user, body);
    }

    @Post('change-password')
    changePassword(@Req() req: Request, @Body(new ValidationPipe()) body: ChangePasswordModel) {
        return this.service.onChangePassword(req.user, body);
    }

    @Get()
    @UseGuards(new RoleGuard(RoleAccount.Admin, RoleAccount.Employee))
    showMembers(@Query(ValidationPipe) query: SearchModel) {
        // tslint:disable-next-line: radix
        query.startPage = parseInt(query.startPage as any);
        // tslint:disable-next-line: radix
        query.limitPage = parseInt(query.limitPage as any);
        return this.service.getMemberItems(query);
    }

    @Post()
    @UseGuards(new RoleGuard(RoleAccount.Admin))
    createMember(@Body(ValidationPipe) body: CreateMemberModel ) {
        return this.service.createMemberItem(body);
    }

    // แสดงข้อมูลรายคน
    @Get(':_id')
    @UseGuards(new RoleGuard(RoleAccount.Admin))
    showMemberbyId(@Param(ValidationPipe) param: ParamMemberModel) {
        // console.log(param)
        return this.service.getMemberItem(param);
    }

    @Put(':_id')
    @UseGuards(new RoleGuard(RoleAccount.Admin))
    updateMember(@Param(ValidationPipe) param: ParamMemberModel, @Body(ValidationPipe) body: UpdateMemberModel) {
        return this.service.updateMemberItem(param._id, body);
    }

    @Delete(':_id')
    @UseGuards(new RoleGuard(RoleAccount.Admin, RoleAccount.Employee))
    deleteMember(@Param(new ValidationPipe()) param: ParamMemberModel) {
        // console.log(param._id);
        return this.service.deleteMemberItem(param._id);
    }
}
