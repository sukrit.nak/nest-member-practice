const isProd = process.env.PROD === 'true';
export const AppEnvironment = {
    dbHost: isProd ? 'mongodb://mongodb-practice/member_db' : 'mongodb://localhost:27027/member_db',
};