import { Injectable, UnauthorizedException } from '@nestjs/common';
import { IMemberDocument } from '../interfaces/member.interface';

import { sign } from 'jsonwebtoken';
import { IAuthen } from '../interfaces/authen.interface';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

@Injectable()
export class JwtAuthenService implements IAuthen {
    constructor(@InjectModel('Member') private MemberCollection: Model<IMemberDocument>) { }

      // ยืนยันตัวตน
      async validateUser({ email }): Promise<IMemberDocument> {
        try {
            return this.MemberCollection.findOne({ email });
        }
        catch (e) { }
        return null;
    }
    // สร้าง secretKey
    static secretKey: string = 'NodeJS Member Workshop';

    // gen token
    async generateAccessToken(member: IMemberDocument) {
        const payload = { email: member.email };
        return sign(payload, JwtAuthenService.secretKey, { expiresIn: 60 * 60 });
    }

}

// tslint:disable-next-line: max-classes-per-file
@Injectable()
export class JwtAuthenStrategy extends PassportStrategy(Strategy) {
    constructor(private readonly authService: JwtAuthenService) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey: JwtAuthenService.secretKey,
        });
    }

    // tslint:disable-next-line: ban-types
    async validate(payload: { email: string }, done: Function) {
        const user = await this.authService.validateUser(payload);
        if (!user) {
            return done(new UnauthorizedException('Unauthorized please login!'), false);
        }
        done(null, user);
    }
}
