import { verify, generate } from 'password-hash';
import { IChangePassword, IMember, ISearch } from './../interfaces/app.interface';
import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { BASE_DIR } from '../main';
import { IMemberDocument } from '../interfaces/member.interface';
import { IProfile, IAccount } from '../interfaces/app.interface';
import { existsSync, mkdirSync, writeFileSync } from 'fs';

@Injectable()
export class MemberService {
    constructor(@InjectModel('Member') private MemberCollection: Model<IMemberDocument>) {
        // const members: IAccount[] = [];
        // for (let i = 0; i <= 100; i++) {
        //     members.push({
        //         firstname: `Firstname ${i}`,
        //         lastname: `Lastname ${i}`,
        //         email: `email${i}@mail.com`,
        //         password: generate(`password-${i}`),
        //         image: '',
        //         position: '',
        //         role: RoleAccount.Member,
        //     });
        // }
        // this.MemberCollection.create(members, (err) => console.log(err))
    }

    // ลบข้อมูลสมาชิก
    deleteMemberItem(memberID: any) {
        return this.MemberCollection.remove({ _id: memberID });
    }


    // แก้ไขข้อมูลสมาชิก
    async updateMemberItem(memberId: any, body: IAccount) {
        const memberUpdate = await this.MemberCollection.findById(memberId);
        if (!memberUpdate) { throw new BadRequestException('ไม่มีผู้ใช้นี้ในระบบ'); }
        try {
            memberUpdate.firstname = body.firstname;
            memberUpdate.lastname = body.lastname;
            memberUpdate.image = this.convertUploadImage(memberId, body.image) || '';
            memberUpdate.position = body.position;
            memberUpdate.email = body.email;
            memberUpdate.role = body.role;
            memberUpdate.updated = new Date();

            if (body.password && body.password.trim() !== '') {
                memberUpdate.password = generate(body.password);

            }
            // แบบไม่เซ็ต uniqe ที่ schema ****
            // const memberItemCount = await this.MemberCollection.count({email: body.email});
            // if (memberItem.email != body.email && memberItemCount > 0) throw BadRequestException('มีอีเมลล์นี้ในระบบแล้ว')
            // แบบไม่เซ็ต uniqe ที่ schema ****
            const updated = await this.MemberCollection.update({ _id: memberId }, memberUpdate);
            if (!updated.ok) { throw new BadRequestException('ไม่สามารถแก้ไขข้อมูลได้'); }
            return await this.MemberCollection.findById(memberId);
        } catch (ex) {
            throw new BadRequestException(ex.message);
        }
    }

    // แสดงข้อมูลรายคน
    async getMemberItem(memberID: any) {
        const memberItem = await this.MemberCollection.findById(memberID, { password: false });
        console.log(memberItem)
        memberItem.image = memberItem.image ? 'http://localhost:3000' + memberItem.image + '?ver=' + Math.random() : '';
        return memberItem;
    }

    // สร้างข้อมูลสมาชิก
    async createMemberItem(body: IAccount) {
        const count = await this.MemberCollection.count({ email: body.email });
        if (count > 0) { throw new BadRequestException('มี email นี้แล้ว'); }

        body.password = generate(body.password);
        console.log(body.image)
        body.image !== null ? this.convertUploadImage(body.email, body.image) : '';
        // body.image = this.convertUploadImage(body.email, body.image) || '';
        const created = await this.MemberCollection.create(body);
        created.password = '';
        return created;
    }

    // แสดงข้อมูลสมาชิก
    async getMemberItems(searchOptions: ISearch): Promise<IMember> {
        let queryItemFunction = () => this.MemberCollection.find({}, { image: false });

        // ส่วนการค้นหา
        if (searchOptions.searchText && searchOptions.searchType) {
            const text = searchOptions.searchText;
            console.log(text);
            const type = searchOptions.searchType;
            console.log(type);
            const conditions = {};
            switch (type) {
                case 'role':
                    conditions[type] = text;
                    console.log(conditions);
                    queryItemFunction = () => this.MemberCollection.find(conditions, { image: false });
                    break;
                case 'updated':
                    queryItemFunction = () => this.MemberCollection.find({
                        updated: {
                            // tslint:disable-next-line: no-string-literal
                            $gt: text['from'],
                            // tslint:disable-next-line: no-string-literal
                            $lt: text['to'],
                        },
                    }, { image: false });
                    // queryItemFunction = () => this.MemberCollection.find({}, { image: false})
                    // .where('updated')
                    // .gt(text['from'])
                    // .lt(text['to'])
                    break;
                default:
                    conditions[type] = new RegExp(text, 'i');
                    queryItemFunction = () => this.MemberCollection.find(conditions, { image: false });
                    break;
            }
        }

        // แบ่งหน้า
        const items = await queryItemFunction()
            .sort({ updated: -1 })
            .skip((searchOptions.startPage - 1) * searchOptions.limitPage)
            .limit(searchOptions.limitPage);
        const totalItems = await queryItemFunction().count({});
        return {
            items,
            totalItems,
        } as IMember;
    }

    //  เปลี่ยนรหัส
    async onChangePassword(member: any, body: IChangePassword) {
        const memberItem = await this.MemberCollection.findById(member.id);
        if (!verify(body.old_pass, memberItem.password)) {
            throw new BadRequestException('รหัสผ่านเดิมไม่ถูกต้อง');
        }

        const updated = await this.MemberCollection.update({ _id: member.id }, {
            password: generate(body.new_pass),
            updated: new Date(),
        } as IAccount);
        return updated;
    }

    // แก้ไขข้อมูลโปรไฟล์
    async onUpdateProfile(member: any, body: IProfile) {
        const updated = await this.MemberCollection.update({ _id: member.id }, {
            firstname: body.firstname,
            lastname: body.lastname,
            position: body.position,
            image:  body.image !== null ? this.convertUploadImage(member.id, body.image) : '',
            updated: new Date(),
        } as IAccount);
        if (!updated.ok) { throw new BadRequestException('ข้อมูลไม่มีการเปลี่ยนแปลง'); }
        const memberItem = await this.MemberCollection.findById(member.id);
        memberItem.password = '';
        memberItem.image = memberItem.image ? 'http://localhost:3000' + memberItem.image + '?ver=' + Math.random() : '';
        return memberItem;
    }

    // แปลงรูปภาพจาก Base64 เป็นไฟล์
    private convertUploadImage(memberID, image: string) {
        try {
            // สร้างโฟลเดอร์ใหม่
            const uploadDir = BASE_DIR + '/uploads';
            if (!existsSync(uploadDir)) { mkdirSync(uploadDir); }

            // ตรวจสอบว่าเป็นชนิด .jpg
            if (image.indexOf('image/jpeg') >= 0) {
                const fileName = `${uploadDir}/${memberID}.jpg`;
                writeFileSync(fileName, new Buffer(image.replace('data:image/jpeg;base64,', ''), 'base64'));
                return fileName.replace(BASE_DIR, '');
            } else if (image.indexOf('image/png') >= 0) {
                const fileName = `${uploadDir}/${memberID}.png`;
                writeFileSync(fileName, new Buffer(image.replace('data:image/png;base64,', ''), 'base64'));
                return fileName.replace(BASE_DIR, '');
            }
            return '';
        } catch (ex) {
            throw new BadRequestException(ex.message);
        }
    }

}
