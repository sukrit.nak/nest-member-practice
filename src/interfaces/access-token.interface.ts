import { Document } from 'mongoose';

export interface IAccessTokenDocument extends Document {
    memberID: any;
    accessToken: string;
    expired: Date;
    created: Date;
}
