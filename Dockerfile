FROM node:10.16.0
COPY . /var/www/node
WORKDIR /var/www/node
EXPOSE 3000
CMD [ "yarn","start:prod" ]